﻿using BOWcfNewsletter;
using DllWcfUtility;
using log4net;
using ServiceNewsletter;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace NewsletterManagement
{
    class NewsletterManagement
    {
        private static ILog log = LogManager.GetLogger("FileLog");
        private static ILog logError = LogManager.GetLogger("FileLogError");

        static void Main(string[] args)
        {
            log.Info("NewsletterManagement.Main Start");

            try
            {
                BeanList<NewsletterChannel> newsletters;
                BeanList<Recipient> recipients;
                ServiceNewsLetterClient newslettersClient;
                ServiceMailsAutomaticosProducto.MailParameter[] parameters;
                ServiceMailsAutomaticosProducto.MailParameter parameter;
                ServiceMailsAutomaticosProducto.ServicioMailAutomaticoProductoClient mailsClient;
                int idEnvio;
                int groupId;
                string groupIdStr;

                groupIdStr = System.Configuration.ConfigurationManager.AppSettings.Get("GROUP_ID");
                if (!int.TryParse(groupIdStr, out groupId))
                {
                    groupId = -1;
                    logError.Error("NewsletterManagement.Main Error No se ha especificado grupo en el fichero de configuración");
                }
                if (groupId > 0)
                {

                    mailsClient = new ServiceMailsAutomaticosProducto.ServicioMailAutomaticoProductoClient();
                    newslettersClient = new ServiceNewsLetterClient();
                   
                    newsletters = newslettersClient.RecuperarNewsletterEnvio();

                    if (newsletters != null && newsletters.beanList != null)
                    {
                        log.Info("NewsletterManagement.Main Número newsletter a enviar: " + newsletters.beanList.Count);
                        foreach (NewsletterChannel news in newsletters.beanList)
                        {
                            log.Info("NewsletterManagement.Main Procesando newsletter: " + news.NewsletterChannelId);
                            recipients = newslettersClient.RecuperarDestinatarios(news.NewsletterChannelId);
                            if (recipients != null && recipients.beanList != null)
                            {
                                log.Info("NewsletterManagement.Main Enviando newsletter " + news.NewsletterChannelId + " a " + recipients.beanList.Count + " destinatarios.");
                                List<string> emails = recipients.beanList.Select(s => s.Email).ToList();
                                string emailsStr = string.Join(",", emails);
                                string rss = NewsletterManagement.GetRSSContent(news.RssURL);
                                parameters = new ServiceMailsAutomaticosProducto.MailParameter[1];
                                parameter = new ServiceMailsAutomaticosProducto.MailParameter();
                                parameter.m_key = ServiceMailsAutomaticosProducto.ConstantsenuAutomaticMailParameterKey.NEWS_ROWS;
                                parameter.m_value = rss;
                                parameters[0] = parameter;
                                idEnvio = mailsClient.EnvioAutomaticoMailsMasivoParametrosGrupo(parameters, emailsStr, groupId, news.Template, news.Sender, news.Subject, 1);
                                newslettersClient.AltaResultadoEnvioNewsletter(news.NewsletterChannelId, idEnvio, 1);
                            }
                            else
                            {
                                logError.Error("NewsletterManagement.Main No hay detinatarios para la newsletter y canal: " + news.NewsletterChannelId);
                            }
                        }
                    }
                    else
                    {
                        log.Info("NewsletterManagement.Main No hay newsletters que enviar");
                    }
                }
                else
                {
                    logError.Error("NewsletterManagement.Main Error, no se ha definido ID de grupo en el fichero de configuración");
                }
            }
            catch (Exception err)
            {
                logError.Error("NewsletterManagement.Main Error: " + err.Message);
            }

            log.Info("NewsletterManagement.Main End");
        }

        public static string GetRSSContent(string url)
        {
            StreamReader textStreamReader;
            Assembly assembly;
            string tableBody;
            try
            {
                XmlReader reader = XmlReader.Create(url);
                SyndicationFeed feed = SyndicationFeed.Load(reader);
                assembly = Assembly.GetExecutingAssembly();
                reader.Close();
                tableBody = string.Empty;
                foreach (SyndicationItem item in feed.Items)
                {
                    textStreamReader = new StreamReader(assembly.GetManifestResourceStream("NewsletterManagement.html.newsRow.html"));
                    if (textStreamReader != null)
                    {
                        String subject = item.Title.Text;
                        String summary = item.Summary.Text;
                        String link = item.Id;
                        String date = string.Format("{0}/{1}/{2}", item.PublishDate.Day, item.PublishDate.Month, item.PublishDate.Year);
                        string row = textStreamReader.ReadToEnd();
                        row = row.Replace("[NEWS_TITLE]", subject);
                        row = row.Replace("[NEWS_SUMMARY]", subject);
                        row = row.Replace("[NEWS_LINK]", link);
                        row = row.Replace("[NEWS_DATE]", date);
                        tableBody += row;

                    }
                }
                return tableBody;

            }
            catch (Exception err)
            {
                logError.Error("NewsletterManagement.GetRSSContent Error: " + err.Message);
            }
            return string.Empty;
        }
    }
}
